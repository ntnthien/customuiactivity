//
//  CustomUIActivity.swift
//  CustomUIActivity
//  This custom CocoaTouch Class will create a share UIActivity that would load a webpage URL if being touched 
//  Created by Do Nguyen on 2/5/16.
//  Copyright © 2016 Zincer. All rights reserved.
//

import UIKit

class CustomUIActivity: UIActivity {
    // Create a unique name for activityType from Class name.
    override func activityType() -> String? {
        return NSStringFromClass(self.classForCoder)
    }
    
    // Name of the activity.
    override func activityTitle() -> String? {
        return "Zincer Activity"
    }
    
    override func activityImage() -> UIImage? {
        return UIImage(named: "activityImage")
    }
    
    override class func activityCategory() -> UIActivityCategory {
        return UIActivityCategory.Action
    }
    
    
    override func prepareWithActivityItems(activityItems: [AnyObject]) {
        for activityItem in activityItems {
            if activityItem.isKindOfClass(NSURL) {
                let url = activityItem as? NSURL
                if UIApplication.sharedApplication().canOpenURL(url!) {
                    UIApplication.sharedApplication().openURL(url!)
                }
            }
            break
        }
    }

    // Perform open action if the activityItems are NSURLs
    override func canPerformWithActivityItems(activityItems: [AnyObject]) -> Bool {
        for activityItem in activityItems {
            if activityItem.isKindOfClass(NSURL) {
                let url = activityItem as? NSURL
                return UIApplication.sharedApplication().canOpenURL(url!)
            }
        }
        return false
    }
}
