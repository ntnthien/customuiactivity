//
//  ViewController.swift
//  CustomUIActivity
//  When touch the action button, the share Activity screen will appear, the zincer Share Activity would open the Zincer home page.
//  Created by Do Nguyen on 2/5/16.
//  Copyright © 2016 Zincer. All rights reserved.
//

import UIKit

class ViewController: UIViewController {


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func action(sender: UIButton) {
        if let zincerURL = NSURL(string: "https://zincer.com") {
            let objectsToShare = [zincerURL]
            let customActivity = CustomUIActivity()
            
            let activityZincer = UIActivityViewController(activityItems: objectsToShare, applicationActivities: [customActivity])
            
            self.presentViewController(activityZincer, animated: true, completion: nil)
        }
    }

}

